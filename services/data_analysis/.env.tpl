#@IgnoreInspection BashAddShebang
### Common
TZ=Asia/Shanghai

### flask

FLASK_APP=manage.py
FLASK_ENV=development
FLASK_RUN_HOST=0.0.0.0
FLASK_RUN_PORT=6000

### DB URI
SQLALCHEMY_DATABASE_URI_ISP=mysql+pymysql://admin:Seer2018@172.20.186.99:13306/isp?charset=utf8mb4
SQLALCHEMY_DATABASE_URI_ISP_CLOUD=mysql+pymysql://admin:Seer2018@172.20.186.99:13306/isp-cloud?charset=utf8mb4

?charset=utf8mb4
MONGO_URI=mongodb://127.0.0.1:27017/kitchen
REDIS_URL=redis://127.0.0.1:6379/0

### gunicorn config

GUNICORN_BIND=0.0.0.0:9000
GUNICORN_WORKER_CLASS=gevent
GUNICORN_TIMEOUT=3000
GUNICORN_GRACEFUL_TIMEOUT=100
GUNICORN_LOGLEVEL=info
GUNICORN_ERRORLOG=-
GUNICORN_ACCESSLOG=-

# other
