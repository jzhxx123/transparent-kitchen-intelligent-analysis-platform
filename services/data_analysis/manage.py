#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: Wu Jiabin
Date: 2020/3/3
Description:
"""

import os

from app import create_app

app = create_app(os.getenv('FLASK_ENV') or 'default')

if __name__ == '__main__':
    app.run()
