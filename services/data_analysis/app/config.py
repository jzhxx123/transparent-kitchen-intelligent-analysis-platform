#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: Wu Jiabin
Date: 2020/3/3
Description: 
"""

import ast
import os

from flask_dotenv import DotEnv


BASEDIR = os.path.abspath(os.path.dirname(__file__))
BASENAME = os.path.basename(BASEDIR)


class BaseConfig:
    DEBUG = True

    FLASK_APP = os.environ.get('FLASK_APP') or 'manage.py'

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI_ISP = 'sqlite://'
    SQLALCHEMY_DATABASE_URI_ISP_CLOUD = 'sqlite://'
    SQLALCHEMY_BINDS = {
        'db_isp': SQLALCHEMY_DATABASE_URI_ISP,
        'db_isp_cloud': SQLALCHEMY_DATABASE_URI_ISP_CLOUD,
    }
    MONGO_URI = None

    @classmethod
    def init_app(cls, app):
        # load env by file
        env_file = os.path.join(BASEDIR, '.env')
        if os.path.exists(env_file):
            env = DotEnv()
            env.init_app(app, env_file)
        else:
            app.logger.debug('=== Not detect .env file ===')
        # load env by os.environ
        cls.reload_env(app)
        cls.update_bind(app)

    @classmethod
    def reload_env(cls, app):
        for env in dir(cls):
            value = os.environ.get(env)

            if value and (not value.startswith('#')):
                if value.lower() in ('true', 'false'):
                    value = value.title()
                    value = ast.literal_eval(value)
                elif value.isdigit():
                    value = ast.literal_eval(value)

                app.config[env] = value
                app.logger.debug(f'{env:<24} {value}')

    @classmethod
    def update_bind(cls, app):
        app.config['SQLALCHEMY_BINDS'] = {
            'db_isp': app.config.get('SQLALCHEMY_DATABASE_URI_ISP'),
            'db_isp_cloud': app.config.get('SQLALCHEMY_DATABASE_URI_ISP_CLOUD'),
        }


class DevelopmentConfig(BaseConfig):
    pass


class ProductionConfig(BaseConfig):
    DEBUG = False


config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig
}
