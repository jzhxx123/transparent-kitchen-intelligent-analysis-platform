#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: Wu Jiabin
Date: 2020/3/4
Description: 
"""

import pandas as pd

from app import db


def pd_query(sql, db_name='db_isp_cloud'):
    """
    :param sql:
    :param db_name: db_isp_cloud(新数据库) / db_isp(旧数据库)
    :return:
    """
    return pd.read_sql_query(sql, db.get_engine(bind=db_name))
