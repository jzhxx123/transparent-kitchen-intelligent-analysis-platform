#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: Wu Jiabin
Date: 2020/3/2
Description: 
"""

from flask import Flask

from app.config import config
from app.extensions import db, mongo


def create_app(config_name):
    app = Flask(__name__, instance_relative_config=True)

    app.config.from_object(config[config_name])
    app.config.from_pyfile('config.py', silent=True)
    app.logger.info('Mode: {}'.format(config_name))
    config[config_name].init_app(app)

    register_extensions(app)
    register_blueprint(app)

    return app


def register_extensions(app):
    db.init_app(app)
    mongo.init_app(app)


def register_blueprint(app):
    pass

