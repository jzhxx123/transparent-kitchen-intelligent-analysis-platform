#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: Wu Jiabin
Date: 2020/3/3
Description: 
"""


class MainType:
    """
    检查力度指数	check_intensity
    检查均衡度	    check_evenness
    问题暴露度	    problem_exposure
    问题整改	    problem_rectification
    评价力度指数	evaluate_intensity
    """
    check_intensity = 1
    check_evenness = 2
    problem_exposure = 3
    problem_rectification = 4
    evaluate_intensity = 5

