#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: Wu Jiabin
Date: 2020/3/4
Description: 
"""

from flask_sqlalchemy import SQLAlchemy as Sa
from flask_pymongo import PyMongo


class SQLAlchemy(Sa):
    def apply_pool_defaults(self, app, options):
        Sa.apply_pool_defaults(self, app, options)
        options['pool_pre_ping'] = True
        options['convert_unicode'] = False  # TODO: suppress warning until fsql=2.4 to delete


db = SQLAlchemy()
mongo = PyMongo()
