#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: Wu Jiabin
Date: 2020/3/11
Description:
"""

import os
import sys
import pprint

from fastapi import FastAPI
from typing import Dict, Any

from application import init_app


"""
USE os.getenv() TO GET ENV VARS IN dev.cfg OR prod.cfg
"""

env: str = os.getenv('ENV')
app_name: str = os.getenv('APP_NAME')
description: str = os.getenv('DESCRIPTION')
version: str = os.getenv('VERSION')

fastapi_cfg: Dict[str, Any] = {
    'debug': env != 'prod',
    'title': app_name,
    'description': description,
    'version': version
}


# init app
app = FastAPI(**fastapi_cfg)

init_app(app)


print('Launching application: %s\n%s' %
      (app_name, pprint.pformat(fastapi_cfg, indent=2, width=50)), file=sys.stderr)
