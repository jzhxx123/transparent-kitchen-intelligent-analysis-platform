ENV=dev
APP_NAME=kitchen-intelligent-analysis-web-api
DESCRIPTION=start_fastapi development environment
VERSION=0.0.1

# database
MONGO_URI=mongodb://user:pwd@host:port/db
SQLALCHEMY_DATABASE_URI_ISP=mysql+pymysql://user:pwd@host:port/db?charset=utf8mb4
SQLALCHEMY_DATABASE_URI_ISP_CLOUD=mysql+pymysql://user:pwd@host:port/db?charset=utf8mb4