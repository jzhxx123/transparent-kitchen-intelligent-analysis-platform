#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     supervision_platform_service
   Author :       hwj
   Description:   分局-智慧监管平台
   date：          2020/3/16下午2:56
   Change Activity: 2020/3/16下午2:56
-------------------------------------------------
"""


def get_category(bureau_id):
    data = {
        'school_category': ['幼儿园', '小学', '初中', '高职', '高中', '大学'],
        'school_count': [396, 476, 379, 120, 243, 5],
        'problem_category': ['人员', '供应商', '流程', '外部信息', '工况', '食材'],
        'problem_count': [212, 312, 300, 351, 292, 260],

    }

    return data


def get_supplier_polyline(bureau_id):
    data = [
        {'year': 2018,
         'month': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
         'violation': [25, 27, 35, 36, 28, 31, 29, 26, 21, 22, 28, 23],
         'survey': [40, 43, 49, 50, 42, 39, 41, 40, 39, 45, 47, 40],
         'aberrant': [55, 57, 60, 67, 58, 59, 62, 60, 64, 62, 68, 60],
         },
        {'year': 2019,
         'month': [1, 2, 3, 4],
         'violation': [24, 28, 25, 31],
         'survey': [37, 41, 39, 42],
         'aberrant': [59, 54, 56, 60]},
    ]
    return data


def get_food_polyline(bureau_id):
    data = [
        {'year': 2018,
         'month': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
         'price': [25, 27, 35, 36, 28, 31, 29, 26, 21, 22, 28, 23],
         'dosage': [40, 43, 49, 50, 42, 39, 41, 40, 39, 45, 47, 40],
         },
        {'year': 2019,
         'month': [1, 2, 3, 4],
         'price': [37, 41, 39, 35],
         'dosage': [24, 28, 25, 29]},
    ]
    return data


def get_school_rank(bureau_id):
    data = {
        'composite': {
            'name': ['万江阳光幼儿园', '东莞市厚街中学', '东莞市厚街卓恩小学', '东莞市竹溪中学', '东莞市厚街丰泰外国语学校'],
            'count': [95, 92, 90, 88, 85]
        },
        'person': {
            'name': ['东莞市厚街镇中心小学', '万江阳光幼儿园', '东莞市竹溪中学', '东莞市厚街卓恩小学', '东莞市厚街丰泰外国语学校'],
            'count': [96, 94, 93, 91, 90]
        },
        'food': {
            'name': ['东莞理工大学', '东莞市厚街丰泰外国语学校', '东莞市厚街卓恩小学', '东莞市厚街菊塘幼儿园', '万江阳光幼儿园'],
            'count': [95, 93, 90, 89, 85]
        },
        'condition': {
            'name': ['东莞市厚街丰泰外国语学校', '东莞市厚街卓恩小学', '东莞市厚街菊塘幼儿园', '万江阳光幼儿园', '东莞理工大学'],
            'count': [97, 94, 93, 92, 90]
        },
        'process': {
            'name': ['东莞市厚街卓恩小学', '东莞市厚街菊塘幼儿园', '东莞市厚街丰泰外国语学校', '万江阳光幼儿园', '东莞市厚街艺园学校'],
            'count': [93, 91, 90, 88, 85]
        },
        'supplier': {
            'name': ['东莞市厚街菊塘幼儿园', '万江阳光幼儿园', '东莞市厚街丰泰外国语学校', '东莞市厚街中学', '东莞市竹溪中学'],
            'count': [96, 95, 92, 91, 89]
        }
    }
    return data


def get_risk_warning(bureau_id):
    data = [
        {'name': '东莞市厚街镇中心小学', 'risk_value': 5, 'address': [113.8, 23.12],
         'risk': [
             {'risk_name': '食品安全不合格', 'risk_count': 1},
             {'risk_name': '供应商行政处罚', 'risk_count': 4}]
         },
        {'name': '东莞市厚街海月学校', 'risk_value': 6, 'address': [113.6, 23.55],
         'risk': [
             {'risk_name': '食品安全不合格', 'risk_count': 1},
             {'risk_name': '食堂员工无带帽子', 'risk_count': 5}]
         },
        {'name': '广东酒店管理职业技术学院', 'risk_value': 3, 'address': [113.4, 23.35],
         'risk': [
             {'risk_name': '食品安全不合格', 'risk_count': 1},
             {'risk_name': '食堂出现老鼠', 'risk_count': 2}]
         },
        {'name': '东莞市竹溪中学', 'risk_value': 1,
         'address': [113.2, 23.15],
         'risk': [
             {'risk_name': '供应商行政处罚', 'risk_count': 1}]
         },
        {'name': '东莞市厚街镇白濠小学', 'risk_value': 3,
         'address': [113.0, 22.9],
         'risk': [
             {'risk_name': '食堂出现老鼠', 'risk_count': 1},
             {'risk_name': '食堂员工无带帽子', 'risk_count': 2}]
         },
        {'name': '广东创新科技职业学院', 'risk_value': 3,
         'address': [113.1, 23.3],
         'risk': [
             {'risk_name': '食堂出现老鼠', 'risk_count': 2},
             {'risk_name': '食堂员工无带帽子', 'risk_count': 1}]
         },
        {'name': '东莞市厚街卓恩小学', 'risk_value': 1,
         'address': [113.3, 23.5],
         'risk': [{'risk_name': '食堂员工无带帽子', 'risk_count': 1}]
         },
        {'name': '东莞市厚街镇溪头小学', 'risk_value': 4,
         'address': [113.5, 23.7],
         'risk': [{'risk_name': '食堂员工无带帽子', 'risk_count': 4}]
         }
    ]
    return data


def get_person_polyline(bureau_id):
    data = {
        'check': [
            {'check_count': 311, 'count': 600, 'check_name': '检查次数'},
            {'check_count': 150, 'count': 220, 'check_name': '晨检次数'},
            {'check_count': 20, 'count': 30, 'check_name': '培训次数'},
            {'check_count': 399, 'count': 500, 'check_name': '出现次数'},
        ],
        'polyline': [
            {'year': 2018,
             'month': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
             'self_check': [35, 45, 49, 35, 50, 45, 55, 38, 40, 45, 33, 29],
             'staff_turnover': [40, 43, 49, 50, 42, 39, 41, 40, 39, 45, 47, 40],
             },
            {'year': 2019,
             'month': [1, 2, 3, 4],
             'self_check': [37, 41, 39, 35],
             'staff_turnover': [44, 35, 33, 38]},
        ]
    }
    return data


def get_status_trend_polyline(bureau_id):
    data = [
        {'year': 2018,
         'month': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
         'heavy': [35, 37, 45, 56, 58, 41, 59, 46, 51, 42, 58, 43],
         'heavier': [45, 47, 65, 66, 58, 51, 49, 56, 51, 52, 58, 53],
         'light': [40, 43, 49, 50, 42, 59, 41, 40, 59, 55, 47, 60],
         'lighter': [55, 57, 60, 67, 58, 59, 62, 60, 64, 62, 68, 60],
         },
        {'year': 2019,
         'month': [1, 2, 3, 4],
         'heavy': [45, 47, 55, 56],
         'heavier': [55, 57, 65, 50],
         'light': [45, 40, 49, 58],
         'lighter': [64, 62, 68, 60],
         }
    ]
    return data


def get_condition_polyline(bureau_id):
    data = [
        {'year': 2018,
         'month': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
         'heavy': [35, 37, 45, 56, 58, 41, 59, 46, 51, 42, 58, 43],
         'heavier': [45, 47, 65, 66, 58, 51, 49, 56, 51, 52, 58, 53]
         },
        {'year': 2019,
         'month': [1, 2, 3, 4],
         'watch': [45, 47, 55, 56],
         'time': [55, 57, 65, 50]
         }
    ]
    return data


def get_process_polyline(bureau_id):
    data = [
        {
            'year': 2018,
            'month': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
            'check': [45, 38, 55, 48, 50, 47, 43, 47, 28, 34, 41, 46],
            'rectification': [65, 60, 70, 67, 58, 54, 63, 67, 62, 63, 56, 71],
            'frequency': [47, 42, 38, 44, 49, 51, 43, 39, 48, 42, 49, 39],
        },
        {
            'year': 2019,
            'month': [1, 2, 3, 4],
            'check': [40, 43, 48, 44],
            'rectification': [59, 63, 67, 62],
            'frequency': [48, 42, 47, 40],
        },

    ]
    return data


def get_polyline(bureau_id):
    supplier = get_supplier_polyline(bureau_id)
    food = get_food_polyline(bureau_id)
    person = get_person_polyline(bureau_id)
    status = get_status_trend_polyline(bureau_id)
    condition = get_condition_polyline(bureau_id)
    process = get_process_polyline(bureau_id)
    data = {
        'supplier': supplier,
        'food': food,
        'person': person,
        'status': status,
        'condition': condition,
        'process': process,
    }
    return data
