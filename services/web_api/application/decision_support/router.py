#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: Wu Jiabin
Date: 2020/3/16
Description: 
"""

from fastapi import Query

from application.decision_support import supervision_platform_service as spv
from application.decision_support import video_service as vs
from application.response import success, error
from application.decision_support import app_router
from application.decision_support.service import navigation, intelligent_analysis


# region 市级地图导航
@app_router.get('/navigation/number_summary', description='', name='市级导航大屏-左侧数字统计')
def navigation_number_summary():
    try:
        data = navigation.get_navigation_number_summary()
    except Exception as e:
        return error(e)
    return success(data)


@app_router.get('/navigation/town_map', description='', name='市级导航大屏-镇区地图')
def navigation_town_map():
    try:
        data = navigation.get_town_map()
    except Exception as e:
        return error(e)
    return success(data)


@app_router.get('/navigation/type_comparison', description='', name='市级导航大屏-类型对比')
def navigation_type_comparison():
    try:
        data = navigation.get_type_comparison()
    except Exception as e:
        return error(e)
    return success(data)


@app_router.get('/navigation/change_trend', description='', name='市级导航大屏-变化趋势')
def navigation_change_trend():
    try:
        data = navigation.get_change_trend()
    except Exception as e:
        return error(e)
    return success(data)


@app_router.get('/navigation/school_rank', description='', name='市级导航大屏-学校排名')
def navigation_school_rank():
    try:
        data = navigation.get_school_rank()
    except Exception as e:
        return error(e)
    return success(data)

# endregion


# region 智能分析预警
@app_router.get('/intelligent_analysis/number_summary', description='', name='智能分析预警-整体统计')
def intelligent_analysis_number_summary():
    try:
        data = intelligent_analysis.get_intelligent_analysis_number_summary()
    except Exception as e:
        return error(e)
    return success(data)


@app_router.get('/intelligent_analysis/emergency', description='', name='智能分析预警-紧急大事件')
def intelligent_analysis_emergency():
    try:
        data = intelligent_analysis.get_emergency()
    except Exception as e:
        return error(e)
    return success(data)


@app_router.get('/intelligent_analysis/risk_ingredient', description='', name='智能分析预警-风险组成')
def intelligent_analysis_risk_ingredient():
    try:
        data = intelligent_analysis.get_risk_ingredient()
    except Exception as e:
        return error(e)
    return success(data)


@app_router.get('/intelligent_analysis/risk_detail', description='', name='智能分析预警-具体风险信息')
def intelligent_analysis_risk_detail():
    try:
        data = intelligent_analysis.get_risk_detail()
    except Exception as e:
        return error(e)
    return success(data)


@app_router.get('/intelligent_analysis/risk_heat_map', description='', name='智能分析预警-风险热力图')
def intelligent_analysis_risk_heat_map():
    try:
        data = intelligent_analysis.get_risk_heat_map()
    except Exception as e:
        return error(e)
    return success(data)


@app_router.get('/intelligent_analysis/inspect_map', description='', name='智能分析预警-自查督查图')
def intelligent_analysis_inspect_map():
    try:
        data = intelligent_analysis.get_inspect_map()
    except Exception as e:
        return error(e)
    return success(data)

# endregion


# region 分局智慧监管平台
@app_router.get('/supervision_platform/category', name='智慧监管平台学校类型及问题种类占比')
def supervision_platform_school(bureau_id: str = Query(..., description='分局id')):
    try:
        data = spv.get_category(bureau_id)
    except Exception as e:
        return error(e)
    return success(data)


@app_router.get('/supervision_platform/rank', name='智慧监管平台单位排名')
def supervision_platform_school(bureau_id: str = Query(..., description='分局id')):
    try:
        data = spv.get_school_rank(bureau_id)
    except Exception as e:
        return error(e)
    return success(data)


@app_router.get('/supervision_platform/risk', name='智慧监管平台风险预警')
def supervision_platform_school(bureau_id: str = Query(..., description='分局id')):
    try:
        data = spv.get_risk_warning(bureau_id)
    except Exception as e:
        return error(e)
    return success(data)


@app_router.get('/supervision_platform/polyline', name='智慧监管平台曲线图')
def supervision_platform_school(bureau_id: str = Query(..., description='分局id')):
    try:
        data = spv.get_polyline(bureau_id)
    except Exception as e:
        return error(e)
    return success(data)


# endregion


# region 分局-视频分析平台
@app_router.get('/video_analysis', name='视频智能分析平台大屏')
def supervision_platform_school(bureau_id: str = Query(..., description='分局id')):
    try:
        data = vs.get_video_analysis(bureau_id)
    except Exception as e:
        return error(e)
    return success(data)

# endregion
