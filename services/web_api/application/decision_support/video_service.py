#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     video_service
   Author :       hwj
   Description:   
   date：          2020/3/17上午9:42
   Change Activity: 2020/3/17上午9:42
-------------------------------------------------
"""


def get_video_analysis(bureau_id):
    school_problem_video = [
        {'station': '洗切消配', 'date': 20191213, 'reason': '无佩戴帽子', 'start_time': '03:16', 'end_time': '4:15',
         'file_id': 121212121221},
        {'station': '洗切消配', 'date': 20191213, 'reason': '无佩戴帽子', 'start_time': '04:16', 'end_time': '4:19',
         'file_id': 121212121221},
        {'station': '洗切消配', 'date': 20191213, 'reason': '无佩戴帽子', 'start_time': '03:16', 'end_time': '4:15',
         'file_id': 121212121221},
        {'station': '洗切消配', 'date': 20191213, 'reason': '无佩戴帽子', 'start_time': '03:16', 'end_time': '4:15',
         'file_id': 121212121221},
        {'station': '洗切消配', 'date': 20191213, 'reason': '无佩戴帽子', 'start_time': '03:16', 'end_time': '4:15',
         'file_id': 121212121221},
        {'station': '灶台操作', 'date': 20191213, 'reason': '无佩戴帽子', 'start_time': '03:16', 'end_time': '4:15',
         'file_id': 121212121221},
        {'station': '灶台操作', 'date': 20191213, 'reason': '无佩戴帽子', 'start_time': '03:16', 'end_time': '4:15',
         'file_id': 121212121221},
        {'station': '灶台操作', 'date': 20191213, 'reason': '无佩戴帽子', 'start_time': '03:16', 'end_time': '4:15',
         'file_id': 121212121221},
        {'station': '灶台操作', 'date': 20191213, 'reason': '无佩戴帽子', 'start_time': '03:16', 'end_time': '4:15',
         'file_id': 121212121221},
        {'station': '灶台操作', 'date': 20191213, 'reason': '无佩戴帽子', 'start_time': '03:16', 'end_time': '4:15',
         'file_id': 121212121221},
        {'station': '灶台操作', 'date': 20191213, 'reason': '无佩戴帽子', 'start_time': '03:16', 'end_time': '4:15',
         'file_id': 121212121221},
        {'station': '台面卫生', 'date': 20191213, 'reason': '有杂物', 'start_time': '03:16', 'end_time': '4:15',
         'file_id': 121212121221},
        {'station': '台面卫生', 'date': 20191213, 'reason': '有杂物', 'start_time': '03:16', 'end_time': '4:15',
         'file_id': 121212121221},
        {'station': '台面卫生', 'date': 20191213, 'reason': '有杂物', 'start_time': '03:16', 'end_time': '4:15',
         'file_id': 121212121221},
    ]

    data = {
        'video_count': 4412,
        'problem_video_count': 312,
        'school_count': 286,
        'school_rank': [
            {'school_name': '东莞市厚街镇中心小学', 'score': 98.5},
            {'school_name': '东莞理工大学', 'score': 96.2},
            {'school_name': '万江阳光幼儿园', 'score': 95.5},
            {'school_name': '东莞市竹溪中学', 'score': 94.3},
            {'school_name': '东莞市厚街卓恩小学', 'score': 94.0},
        ],
        'problem_video': [
            {'school_name': '东莞市厚街镇中心小学',
             'location': [118.2, 24.1],
             'school_problem_video_count': 35,
             'school_problem_video': school_problem_video},
            {'school_name': '万江阳光幼儿园',
             'location': [118.4, 24.2],
             'school_problem_video_count': 35,
             'school_problem_video': school_problem_video},
            {'school_name': '东莞理工大学',
             'location': [118.6, 24.3],
             'school_problem_video_count': 35,
             'school_problem_video': school_problem_video},
            {'school_name': '东莞市厚街卓恩小学',
             'location': [118.6, 24.4],
             'school_problem_video_count': 35,
             'school_problem_video': school_problem_video},
            {'school_name': '东莞市厚街菊塘幼儿园',
             'location': [118.8, 24.5],
             'school_problem_video_count': 35,
             'school_problem_video': school_problem_video},
            {'school_name': '东莞市厚街海月学校',
             'location': [118.9, 24.7],
             'school_problem_video_count': 35,
             'school_problem_video': school_problem_video},
        ]

    }
    return data
