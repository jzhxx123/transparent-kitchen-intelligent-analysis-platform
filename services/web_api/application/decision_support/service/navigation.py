#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: Wu Jiabin
Date: 2020/3/16
Description: 
"""

from random import randint


def get_navigation_number_summary():
    data = {
        'no_accident_days': 23241,
        'school': 1700,
        'video': 15238,
        'food_security_manager': 523,
        'inspector': 402,
        'monitoring_hours': 53541
    }
    return data


def get_town_map():
    data = [
        {'label': '麻涌镇', 'risk_index': randint(0, 200)},
        {'label': '望牛墩', 'risk_index': randint(0, 200)},
        {'label': '中堂镇', 'risk_index': randint(0, 200)},
        {'label': '桥头镇', 'risk_index': randint(0, 200)},
        {'label': '莞城区', 'risk_index': randint(0, 200)},
        {'label': '寮步镇', 'risk_index': randint(0, 200)},
        {'label': '大岭山', 'risk_index': randint(0, 200)},
        {'label': '东城区', 'risk_index': randint(0, 200)},
        {'label': '大朗镇', 'risk_index': randint(0, 200)},
        {'label': '虎门镇', 'risk_index': randint(0, 200)},
        {'label': '道滘镇', 'risk_index': randint(0, 200)},
        {'label': '清溪镇', 'risk_index': randint(0, 200)},
        {'label': '樟木头', 'risk_index': randint(0, 200)},
        {'label': '塘厦镇', 'risk_index': randint(0, 200)},
        {'label': '凤岗镇', 'risk_index': randint(0, 200)},
        {'label': '东城区', 'risk_index': randint(0, 200)},
        {'label': '南城区', 'risk_index': randint(0, 200)}
    ]
    return data


def get_type_comparison():
    data = {
        'staff': 90,
        'food_ingredient': 70,
        'supplier': 69,
        'process': 55,
        'work_situation': 46,
        'external_info': 86
    }
    return data


def get_change_trend():
    data = {
        'safety_production_info': {'count': 2424, 'diff': 24, 'percent': 13, 'plus_or_minus': '+'},
        'safety_inspect_info': {'count': 34, 'diff': 13, 'percent': 20, 'plus_or_minus': '+'},
        'warning_problem': {'count': 14, 'diff': 2, 'percent': 4, 'plus_or_minus': '-'},
        'principal_evaluate': {'count': 64, 'diff': 5, 'percent': 9, 'plus_or_minus': '+'}
    }
    return data


def get_school_rank():
    data = {
        'top': [
            {'label': '东莞厚街中学', 'diff': 3},
            {'label': '东莞市第一中学', 'diff': -1},
            {'label': '东莞中学松山湖学校', 'diff': 7},
            {'label': '北师大翰林学校', 'diff': 12},
            {'label': '东城高级中学', 'diff': 22}],
        'bottom': [
            {'label': '东华中学', 'diff': -22},
            {'label': '东莞石排中学', 'diff': -12},
            {'label': '东莞塘厦中学', 'diff': -45},
            {'label': '东莞第三附属中学', 'diff': -31},
            {'label': '东莞长安中学', 'diff': 1}
        ]}
    return data
