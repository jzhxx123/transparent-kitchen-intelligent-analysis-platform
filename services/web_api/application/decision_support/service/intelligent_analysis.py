#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: Wu Jiabin
Date: 2020/3/16
Description: 
"""


def get_intelligent_analysis_number_summary():
    data = {
        'no_response_days': 1942,
        'inspect': {'total_count': 675, 'total_diff': 3, 'frequency': -32, 'distribution': 3, 'problem': 34},
        'risk_problem': {'total_count': 256, 'total_diff': 3, 'exception': -12, 'distribution': 11, 'problem': 5},
        'rectify': {'finished': 431, 'unfinished': 121},
        'security_incident': {'total_count': 112, 'total_diff': -7, 'frequency': -7, 'distribution': 9, 'problem': 12}
    }
    return data


def get_emergency():
    data = [
        {'title': '阳光小学发生食安大事件-食品安全检查不合格'},
        {'title': '清溪第三中心小学发生食安大事件-食品安全检查不合格'},
        {'title': '东华中学松山湖校区发生食安大事件-食品安全检查不合格'},
        {'title': '翰林中学发生食安大事件-食品安全检查不合格'}
    ]
    return data


def get_risk_ingredient():
    data = {'supplier': 13,
            'staff': 10,
            'process': 45,
            'work_situation': 25,
            'food_ingredient': 17}
    return data


def get_risk_detail():
    data = {
        'video': {'count': 3775, 'diff': 10},
        'member': {'count': 1623, 'diff': 4},
        'food_ingredient': {'count': 231, 'diff': -8},
        'process': {'count': 1021, 'diff': 1},
        'work_situation': {'count': 1945, 'diff': -12},
        'supplier': {'count': 451, 'diff': 32},
        'school': {'increased': 45, 'normal': 345, 'normal_diff': 11, 'exceptional': 851, 'exceptional_diff': 5}
    }
    return data


def get_risk_heat_map():
    position = [
        {'position': (124, 91), 'value': 76},
        {'position': (123, 91), 'value': 45},
        {'position': (123, 91), 'value': 77},
        {'position': (121, 93), 'value': 88},
        {'position': (123, 89), 'value': 92},
        {'position': (123, 91), 'value': 62}
    ]
    rank = [
        {'label': '万江阳光幼儿园', 'value': 96},
        {'label': '东莞市外国语小学', 'value': 88},
        {'label': '东莞第三阳光中学', 'value': 82},
        {'label': '东莞理工大学', 'value': 78},
        {'label': '塘厦第一中学', 'value': 77}
    ]
    return {'position': position, 'rank': rank}


def get_inspect_map():
    position = [
        {'position': (124, 91), 'status': 1},
        {'position': (123, 91), 'status': 1},
        {'position': (123, 91), 'status': 1},
        {'position': (121, 93), 'status': 1},
        {'position': (123, 89), 'status': 0},
        {'position': (123, 91), 'status': 2}
    ]
    rank = [
        {'label': '万江阳光幼儿园', 'value': 96},
        {'label': '东莞市外国语小学', 'value': 88},
        {'label': '东莞第三阳光中学', 'value': 82},
        {'label': '东莞理工大学', 'value': 78},
        {'label': '塘厦第一中学', 'value': 77}
    ]
    return {'position': position, 'rank': rank}
