#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: Wu Jiabin
Date: 2020/3/12
Description: 
"""
from fastapi import Query

from application.manage_backend import app_router
from application.response import success, error
from application.manage_backend.service import supervise, school
from application.manage_backend.enum import ProblemClass


@app_router.get('/health')
def health_check():
    return success({
        'name': 'backend_manage'
    })


# region 学校主页
@app_router.get('/school/person_order', name='学校主页食安员及采购')
def school_person_info(school_id: str = Query(..., description='学校id')):
    try:
        data = school.get_person_order(school_id)
    except Exception as e:
        return error(e)
    return success(data)


@app_router.get('/school/video', name='学校主页视频')
def school_video_info(school_id: str = Query(..., description='学校id')):
    try:
        data = school.get_school_video(school_id)
    except Exception as e:
        return error(e)
    return success(data)


@app_router.get('/school/check_risk', name='学校主页检查及风险问题')
def school_check_info(school_id: str = Query(..., description='学校id')):
    try:
        data = school.get_check_risk(school_id)
    except Exception as e:
        return error(e)
    return success(data)


@app_router.get('/school/map', name='学校主页地图')
def school_problem_info(school_id: str = Query(..., description='学校id')):
    try:
        data = school.get_map(school_id)
    except Exception as e:
        return error(e)
    return success(data)


# endregion


# region 监管端
@app_router.get('/single/supervise_site/class_summary', description='为单机版提供', name='监督端后台主页分类概览')
def supervise_site_home_class_summary(tenant_id: str = Query(default=..., description='租户id')):
    try:
        data = supervise.get_supervise_site_class_summary(tenant_id)
    except Exception as e:
        return error(e)
    return success(data)


@app_router.get('/single/supervise_site/problem_list', description='为单机版提供', name='监督端后台主页问题列表')
def supervise_site_home_problem_list(tenant_id: str = Query(default=..., description='租户id'),
                                     class_name: ProblemClass = Query(default=None, description='分类名称')):
    try:
        data = supervise.get_supervise_site_problem_list(tenant_id, class_name)
    except Exception as e:
        return error(e)
    return success(data)


@app_router.get('/single/supervise_site/school_rank', description='为单机版提供', name='监督端后台主页学校排名')
def supervise_site_home_school_rank(tenant_id: str = Query(default=..., description='租户id')):
    try:
        data = supervise.get_supervise_site_school_rank(tenant_id)
    except Exception as e:
        return error(e)
    return success(data)


@app_router.get('/single/supervise_site/warning_trend', description='为单机版提供', name='监督端后台主页预警趋势')
def supervise_site_home_warning_trend(tenant_id: str = Query(default=..., description='租户id'),
                                      class_name: ProblemClass = Query(default=None, description='分类名称')):
    try:
        data = supervise.get_supervise_site_warning_trend(tenant_id, class_name)
    except Exception as e:
        return error(e)
    return success(data)


@app_router.get('/single/supervise_site/detail_problem_stats', description='为单机版提供', name='监督端后台详细问题统计')
def supervise_site_home_class_problem_stats(tenant_id: str = Query(default=..., description='租户id'),
                                            class_name: ProblemClass = Query(default=..., description='分类名称')):
    try:
        data = supervise.get_supervise_site_class_problem_stats(tenant_id, class_name)
    except Exception as e:
        return error(e)
    return success(data)

# endregion
