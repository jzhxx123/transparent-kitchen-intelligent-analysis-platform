#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: Wu Jiabin
Date: 2020/3/12
Description: 
"""


def get_supervise_site_class_summary(tenant_id):
    data = {'school': {'total': 1102, 'diff': '+4', 'problem': 765},
            'supplier': {'total': 843, 'diff': '-8', 'problem': 142},
            'self_inspect': {'total': 1952, 'diff': '-13', 'problem': 203},
            'supervise': {'total': 67, 'diff': '+12', 'problem': 22},
            'video': {'total': 2345, 'diff': '+30', 'problem': 973},
            'rectify': {'total': 689, 'diff': '+2', 'problem': 26}
            }
    return data


def get_supervise_site_problem_list(tenant_id, class_name=None):
    data = [
        {'label': '供应品异动', 'class': '供应商', 'count': 6, 'time': '2020-03-06 14:56'},
        {'label': '食品安全问题', 'class': '供应商', 'count': 12, 'time': '2020-03-06 14:56'},
        {'label': '不带帽子', 'class': '视频', 'count': 19, 'time': '2020-03-06 14:56'},
        {'label': '证件过期', 'class': '自查', 'count': 66, 'time': '2020-03-06 14:56'},
        {'label': '抽查不合格', 'class': '督查', 'count': 23, 'time': '2020-03-05 14:56'},
        {'label': '自查不合格', 'class': '学校', 'count': 44, 'time': '2020-03-05 14:56'},
        {'label': '违规次数', 'class': '供应商', 'count': 80, 'time': '2020-03-04 14:56'},
        {'label': '整改次数', 'class': '学校', 'count': 38, 'time': '2020-03-04 14:56'},
        {'label': '证件不合格', 'class': '供应商', 'count': 9, 'time': '2020-03-04 14:56'}
    ]
    return data


def get_supervise_site_school_rank(tenant_id):
    data = {'top': ['东莞厚街中学', '东莞市第一中学', '东莞中学松山湖学校', '北师大翰林学校', '东城高级中学'],
            'bottom': ['东华中学', '东莞石排中学', '东莞塘厦中学', '东莞第三附属中学', '东莞长安中学']}
    return data


def get_supervise_site_warning_trend(tenant_id, class_name):
    data = {'year': 2019,
            'score': [40, 50, 60, 60, 77, 64, 59, 71, 80, 85, 75, 75],
            'months': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]}
    return data


def get_supervise_site_class_problem_stats(tenant_id, class_name):
    data = {'class': ['证件过期', '食品安全问题', '证据更换频次', '供应品类异动', '违规次数', '违规次数', '抽查次数'],
            'count': [98, 60, 70, 59, 50, 89, 55]}
    return data


def get_supervise_site_stats_data_by_class(tenant_id, class_name):
    data = {}

    problem_list = [
        {'label': '供应品异动', 'class': '供应商', 'count': 6, 'time': '2020-03-06 14:56'},
        {'label': '食品安全问题', 'class': '供应商', 'count': 12, 'time': '2020-03-06 14:56'},
        {'label': '不带帽子', 'class': '视频', 'count': 19, 'time': '2020-03-06 14:56'},
        {'label': '证件过期', 'class': '自查', 'count': 66, 'time': '2020-03-06 14:56'},
        {'label': '抽查不合格', 'class': '督查', 'count': 23, 'time': '2020-03-05 14:56'},
        {'label': '自查不合格', 'class': '学校', 'count': 44, 'time': '2020-03-05 14:56'},
        {'label': '违规次数', 'class': '供应商', 'count': 80, 'time': '2020-03-04 14:56'},
        {'label': '整改次数', 'class': '学校', 'count': 38, 'time': '2020-03-04 14:56'},
        {'label': '证件不合格', 'class': '供应商', 'count': 9, 'time': '2020-03-04 14:56'}
    ]
    data['problem_list'] = problem_list

    warning_trend = {'year': 2019,
                     'score': [40, 50, 60, 60, 77, 64, 59, 71, 80, 85, 75, 75],
                     'months': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]}
    data['warning_trend'] = warning_trend

    problem_class = {'class': ['证件过期', '食品安全问题', '证据更换频次', '供应品类异动', '违规次数', '违规次数', '抽查次数'],
                     'count': [98, 60, 70, 59, 50, 89, 55]}
    data['problem_class'] = problem_class

    return data
