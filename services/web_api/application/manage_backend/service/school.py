#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     common
   Author :       hwj
   Description:   
   date：          2020/3/12下午3:38
   Change Activity: 2020/3/12下午3:38
-------------------------------------------------
"""
from datetime import datetime as dt


def get_person_order(school_id):
    person = {
        'name': '郭晓',
        'avatar': 1235469486132842498,
        'age': 29,
        'gender': '男',
        'address': '广东省东莞市东城区鸿福路',
        'tel': 15089295666,
        'id_card': '441381199008172222',
        'begin_time': dt.now().date().strftime('%Y-%m-%d'),
        'certificate': '合格',
        'test_pass': 3,
        'test_count': 4,
        'test_level': '不合格',
        'certificate_picture': [
            1235476188500611074, 1235732619854348290, 1235735515064958977, 1235738385134911490]
    }
    aberrant_order = [
        {'supplier_name': '东莞市松山湖金雷蔬菜经营部', 'food': '肉类', 'purchase_person': '吴家鸿', 'order_id': 1238341420406804482},
        {'supplier_name': '东莞市集嘉供应链有限公司', 'food': '冻品', 'purchase_person': '黄晓琪', 'order_id': 1238341995643015170},
        {'supplier_name': '广东喜旋餐饮管理有限公司', 'food': '粮油', 'purchase_person': '孙成国', 'order_id': 1238342293442793473},
        {'supplier_name': '东莞菁菁优品食材配送有限公司', 'food': '调味品', 'purchase_person': '张耿', 'order_id': 1238342682770673665},
        {'supplier_name': '广东信农供应链股份有限公司', 'food': '肉类', 'purchase_person': '王斯', 'order_id': 1238342897997189122},
        {'supplier_name': '东莞市东城怀新蔬菜经营部', 'food': '肉类', 'purchase_person': '章测', 'order_id': 1238343908543434754},
        {'supplier_name': '广东信农供应链股份有限公司', 'food': '海鲜', 'purchase_person': '李夏', 'order_id': 1238344200592822274},
        {'supplier_name': '东莞市松山湖金雷蔬菜经营部', 'food': '蔬菜', 'purchase_person': '周杰', 'order_id': 1238344395703455746},
    ]
    order = {
        'count': 150,
        'aberrant_count': 14,
        'aberrant_order': aberrant_order
    }
    data = {'person': person, 'order': order}

    return data


def get_school_video(school_id):
    video = {
        'count': 1999,
        'aberrant_count': 100,
        'category': {'name': ['不戴帽子', '洗消配', '灶台操作', '台面卫生'],
                     'count': [40, 30, 25, 5]}
    }
    return video


def get_check_risk(school_id):
    check = {
        'daily': 0,
        'special': 3,
        'process': 8,
        'rectify': 1,
    }
    problem = {'name': ['证件过期', '食品安全', '证件更换频次', '供应品类移动', '违规次数'],
               'count': [60, 45, 15, 20, 30]
               }
    data = {
        'check': check,
        'problem': problem,
    }
    return data


def get_school_point(school_id):
    point_list = [
        {'name': '文化路美食街道', 'latitude': [113.8, 23.12], 'count': 60},
        {'name': '国奥路德新商城', 'latitude': [113.6, 23], 'count': 35, },
        {'name': '祈福超市', 'latitude': [113.9, 23.22], 'count': 15, }
    ]
    point = {'point': point_list}
    return point


def get_school_radar(school_id):
    radar = {'name': ['人员', '供应商', '流程', '外部信息', '工况', '食材'],
             'count': [85, 90, 85, 85, 90, 85, 90]
             }
    return radar


def get_map(school_id):
    point = get_school_point(school_id)
    radar = get_school_point(school_id)
    data = {
        'point': point,
        'radar': radar
    }
    return data
