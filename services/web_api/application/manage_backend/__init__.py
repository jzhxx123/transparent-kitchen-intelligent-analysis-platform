#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: Wu Jiabin
Date: 2020/3/12
Description:  应用端后台数据接口
"""

from fastapi import APIRouter

app_router = APIRouter()

from . import router
