#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: Wu Jiabin
Date: 2020/3/12
Description: 
"""

from enum import Enum


class ProblemClass(str, Enum):
    school = 'school'
    supplier = 'supplier'
    self_inspect = 'self_inspect'
    supervise = 'supervise'
    video = 'video'
    rectify = 'rectify'
