#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: Wu Jiabin
Date: 2020/3/12
Description: 
"""

import pandas as pd

from application import db, mongo, logger


def pd_query(sql, db_name='db_isp_cloud'):
    """
    :param sql:
    :param db_name: db_isp_cloud(新数据库) / db_isp(旧数据库)
    :return:
    """
    return pd.read_sql_query(sql, db.get_engine(db_name))


def write_bulk_mongo(collection, result):
    _len = len(result)
    logger.debug(
        '├── writing {} documents to 「{}」 in {} bulks.'.format(
            _len, collection, (_len // 10000 + 1)))
    for x in range(0, _len, 10000):
        end_index = x + 10000 if (x + 10000) < _len else _len
        bulk_data = result[x:end_index]
        logger.debug('|   └── writing bulk-{} data.'.format(
            int(x / 10000 + 1)))
        mongo.db[collection].insert_many(bulk_data)
    logger.debug('├── writing: complete')
