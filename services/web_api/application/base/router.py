#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: Wu Jiabin
Date: 2020/3/11
Description: 
"""

from application.response import success
from application.settings import settings
from application.base import app_router

app_name = settings.APP_NAME


@app_router.get('/health')
def health_check():
    return success({
        'name': app_name
    })
