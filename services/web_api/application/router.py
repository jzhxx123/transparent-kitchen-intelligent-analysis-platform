#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: Wu Jiabin
Date: 2020/3/11
Description:
"""


from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from application.base.router import app_router as base_router
from application.manage_backend import app_router as manage_backend_router
from application.decision_support import app_router as decision_support_router


def register_routers(app: FastAPI) -> None:
    """
    register controller callbacks to routers
    NOTE: /docs & /redoc are internal routers of fastapi
    :param app: fastapi app
    :return: None
    """
    app.include_router(base_router, prefix='/base')
    app.include_router(manage_backend_router, prefix='/backend', tags=['管理后台接口'])
    app.include_router(decision_support_router, prefix='/decision_support', tags=['决策支持大屏接口'])


def register_middleware(app: FastAPI) -> None:
    """
    register middlewares
    :param app: fastapi app
    :return: None
    """
    app.add_middleware(
        CORSMiddleware,
        allow_origins=['*'],
        allow_methods=["*"],
        allow_headers=["*"],
    )

