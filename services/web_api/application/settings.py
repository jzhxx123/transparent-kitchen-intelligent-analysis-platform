#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: Wu Jiabin
Date: 2020/3/11
Description: 
"""

from pydantic import BaseSettings


class Settings(BaseSettings):
    APP_NAME = ''
    TZ = 'Asia / Shanghai'
    MONGO_URI = ''
    SQLALCHEMY_DATABASE_URI_ISP = 'sqlite://'
    SQLALCHEMY_DATABASE_URI_ISP_CLOUD = 'sqlite://'
    SQLALCHEMY_BINDS = {}

    LOG_DIR = 'log'

    def reload_binds(self):
        self.SQLALCHEMY_BINDS = {
            'db_isp': self.SQLALCHEMY_DATABASE_URI_ISP,
            'db_isp_cloud': self.SQLALCHEMY_DATABASE_URI_ISP_CLOUD
        }


settings = Settings()


