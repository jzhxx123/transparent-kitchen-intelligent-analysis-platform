#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: Wu Jiabin
Date: 2020/3/11
Description: 
"""
import logging

import pymongo
from pymongo import uri_parser
from sqlalchemy import create_engine

from application.settings import settings


class Mongo:

    def __init__(self):
        self.cx = None
        self.db = None

    def init_app(self, app, *args, **kwargs):
        uri = settings.MONGO_URI

        if uri is not None:
            args = tuple([uri] + list(args))
        else:
            raise ValueError(
                "You must specify a URI or set the MONGO_URI Flask config variable",
            )

        parsed_uri = uri_parser.parse_uri(uri)
        database_name = parsed_uri["database"]

        # Try to delay connecting, in case the app is loaded before forking, per
        # http://api.mongodb.com/python/current/faq.html#is-pymongo-fork-safe
        kwargs.setdefault("connect", False)

        self.cx = pymongo.MongoClient(*args, **kwargs)
        if database_name:
            self.db = self.cx[database_name]


class SqlAlchemy:

    def __init__(self):
        self._engine = {}

    def init_app(self, app):
        settings.reload_binds()
        for k, v in settings.SQLALCHEMY_BINDS.items():
            self._engine[k] = create_engine(v)

    def get_engine(self, name):
        return self._engine.get(name)


mongo = Mongo()
db = SqlAlchemy()
logger = logging.getLogger(__name__)


def init_mysql():
    pass


def init_app(app):
    from application import router
    # 注册路由
    router.register_routers(app)
    # 注册中间件
    router.register_middleware(app)

    # 初始化一些扩展
    mongo.init_app(app)
    db.init_app(app)

